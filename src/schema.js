const { GraphQLUpload } = require('graphql-upload')
const {
  makeSchema,
  nonNull,
  objectType,
  stringArg,
  enumType,
  mutationType,
  queryType,
  arg,
  nullable,
} = require('nexus')
const { image } = require('./helpers/cloudinary')
const {Upload, uploadFile} = require('./utils')

const Experience = objectType({
  name: 'Experience',
  definition(t) {
    t.string("firstname")
    t.string("lastname")
    t.string("type")
    t.string("location")
    t.string("story")
    t.string("image")
    t.string("createdAt")
    t.string("updatedAt")
  }
})

const Query = queryType({
  definition(t) {
    t.list.field('allExp', {
      type: 'Experience',
      resolve: (_parent, _args, context) => {
        return context.prisma.experience.findMany({
          orderBy: [
            {
              createdAt: 'desc'
            }
          ]
        })
      },
    })
  },
})

const Mutation = mutationType({
  definition(t) {
    t.field('addExp', {
      type: 'Experience',
      args: {
        firstname: nonNull(stringArg()),
        lastname: nonNull(stringArg()),
        type: nonNull(stringArg()),
        location: stringArg(),
        story: nonNull(stringArg()),
        image: nullable(
          arg({
          type: GraphQLUpload,
          require: false
        })
        ) 
      },
      resolve: async (_, args, context) => {

        if(args.image){
const img = await uploadFile(args.image)

        return await context.prisma.experience.create({
          data: {
            firstname: args.firstname,
            lastname: args.lastname,
            type: args.type,
            location: args.location,
            story: args.story,
            image: img.secure_url
          },
        })
        }else{
          return await context.prisma.experience.create({
            data: {
              firstname: args.firstname,
              lastname: args.lastname,
              type: args.type,
              location: args.location,
              story: args.story,
              image: undefined
            },
          })
        }
       
      },
    })
  }
})




    // const Type = enumType({
    //   name: 'Type',
    //   members: ['vendor', 'customer'],
    // })


    const schema = makeSchema({
      types: [
        Query,
        Mutation,
        Experience

      ],
      outputs: {
        schema: __dirname + '/../schema.graphql',
        typegen: __dirname + '/generated/nexus.ts',
      },
      sourceTypes: {
        modules: [
          {
            module: '@prisma/client',
            alias: 'prisma',
          },
        ],
      },
    })

    module.exports = {
      schema: schema
    }
