
const cloudinary = require('./helpers/cloudinary')
const { GraphQLUpload } = require('graphql-upload');
const { asNexusMethod } = require('nexus')
const Upload = asNexusMethod(GraphQLUpload, 'upload')



const uploadFile = async (file) => {
  return new Promise((resolve, reject) => {
    cloudinary.uploader.upload(file,{invalidate:true},function (
      error,
      result
    ) {
      if (result) {
        resolve(result)
      } else {
        reject(error);
      }
    });
  })
};

module.exports = {
  
  uploadFile,
  Upload,
}
